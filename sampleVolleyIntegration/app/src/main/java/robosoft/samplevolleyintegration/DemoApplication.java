package robosoft.samplevolleyintegration;

import android.app.Application;
import android.content.Context;

/**
 * Created by Supriya A on 3/14/2017.
 */

public class DemoApplication extends Application{
    private static DemoApplication mInstance;
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        mInstance = this;

        //Initialise the volley
        CLCVolley.init(this);
    }

    public static DemoApplication getInstance() {
        return mInstance;
    }

}
