/*
 * Copyright © 2016, National Media Group Ltd. All rights reserved.
 * Written under contract by Robosoft Technologies Pvt. Ltd.
 */

package robosoft.samplevolleyintegration.entity;

import com.google.gsonhtcfix.annotations.SerializedName;


public class CPSStatusError implements IPSDataModel {

    private static final long serialVersionUID = 1L;

    @SerializedName("result")
    private String mResult;

    @SerializedName("message")
    private CPSError mMessage;

    @SerializedName("code")
    private int mCode;

    public String getmResult() {
        return mResult;
    }

    public CPSError getmMessage() {
        return mMessage;
    }

    public int getmCode() {
        return mCode;
    }


}
