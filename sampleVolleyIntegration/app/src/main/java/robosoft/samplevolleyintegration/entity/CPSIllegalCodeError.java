package robosoft.samplevolleyintegration.entity;

import com.google.gsonhtcfix.annotations.SerializedName;


public class CPSIllegalCodeError implements IPSDataModel{


	private static final long serialVersionUID = 1L;
	
	@SerializedName("error")
	private String mError;
	
	@SerializedName("status")
	private CPSStatusError mStatusError;


	public CPSStatusError getStatusError() {
		return mStatusError;
	}

	public String getError() {
		return mError;
	}
	
	public void setStatusError(CPSStatusError statusError) {
		this.mStatusError = statusError;
	}
	
	

}
