package robosoft.samplevolleyintegration.entity;

import com.google.gsonhtcfix.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Supriya A on 3/23/2017.
 */

public class CPSNewOrder implements IPSDataModel {

    @SerializedName("neworders")
    private ArrayList<CPSNewOrderItem> orderitem;


    public ArrayList<CPSNewOrderItem> getOrderitem() {
        return orderitem;
    }

}
