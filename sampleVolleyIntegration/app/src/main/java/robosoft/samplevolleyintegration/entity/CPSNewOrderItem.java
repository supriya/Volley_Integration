package robosoft.samplevolleyintegration.entity;

import com.google.gsonhtcfix.annotations.SerializedName;

/**
 * Created by Supriya A on 3/21/2017.
 */

public class CPSNewOrderItem implements IPSDataModel {

    @SerializedName("orderno")
    private String orderNo;


    @SerializedName("quantity")
    private String quantityAssigned;

    @SerializedName("Destination")
    private String destination;

    @SerializedName("no_oftruck")
    private int nooftruck;

    @SerializedName("Truck_Type")
    private String trucktype;

    @SerializedName("pick_up_date")
    private String pickupdate;

    @SerializedName("delivery_date")
    private String delivarydate;

    @SerializedName("pick_up_time")
    private String pickuptime;

    @SerializedName("trucks_alloted")
    private int trucksAlloted;

    @SerializedName("trucks_pending")
    private int trucksPending;

    @SerializedName("Transporter_name")
    private String transporterName;

    @SerializedName("order_status")
    private String orderStatus;

    private int mSelectedNoOfTruck;

    public String getOrderNo() {
        return orderNo;
    }

    public String getQuantityAssigned() {
        return quantityAssigned;
    }

    public String getDestination() {
        return destination;
    }

    public int getNooftruck() {
        return nooftruck;
    }

    public String getTrucktype() {
        return trucktype;
    }

    public String getPickupdate() {
        return pickupdate;
    }

    public String getDelivarydate() {
        return delivarydate;
    }

    public String getPickuptime() {
        return pickuptime;
    }


    public void setSelectedNoOfTruck(int mSelectedNoOfTruck) {
        this.mSelectedNoOfTruck = mSelectedNoOfTruck;
    }

    public int getSelectedNoOfTruck() {
        return mSelectedNoOfTruck;
    }

    public int getTrucksAlloted() {
        return trucksAlloted;
    }

    public int getTrucksPending() {
        return trucksPending;
    }

    public String getTransporterName() {
        return transporterName;
    }

    public String getOrderStatus() {
        return orderStatus;
    }
}
