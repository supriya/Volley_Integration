package robosoft.samplevolleyintegration;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import robosoft.samplevolleyintegration.entity.CPSNewOrder;
import robosoft.samplevolleyintegration.entity.IPSDataModel;
import robosoft.samplevolleyintegration.net.CPSGsonGetRequest;

public class MainActivity extends AppCompatActivity implements Response.ErrorListener, Response.Listener<IPSDataModel> {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Log.d("TAG","ON ERROR ---" + error);
    }

    @Override
    public void onResponse(IPSDataModel response) {
        if(response instanceof CPSNewOrder) {
            CPSNewOrder order = (CPSNewOrder) response;
            Log.d("TAG", "ON RESPONSE --" + order.getOrderitem().size());
        }
    }

    public void apicall(View view) {
        CLCVolley.addToRequestQueue(new CPSGsonGetRequest("https://dl.dropbox.com/s/p37pujwfdjgz5ky/neworder.json?dl=0",this,this,new CPSNewOrder()));
    }
}
