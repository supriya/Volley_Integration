package robosoft.samplevolleyintegration;

/**
 * Created by Supriya A on 4/17/2017.
 */
public class CPSConstants {

    /* ERROR CODE*/
    public static final int ERROR_CODE_499 = 499;
    public static final int ERROR_CODE_449 = 449;
    public static final int ERROR_CODE_502 = 502;
    public static final int ERROR_CODE_503 = 503;
    public static final int ERROR_CODE_504 = 504;
    public static final int ERROR_CODE_401 = 401;
    public static final int ERROR_CODE_410 = 410;
    public static final int ERROR_CODE_400 = 400;
    public static final String FAILURE = "failure";
    public static final String FAILURE_ERROR = "failure_error";
    public static final int ERROR_CODE_403 = 403;
    public static final String PARSE_ERROR = "parsing_error";
    public static final String NETWORK_ERROR = "NetworkError";
}
