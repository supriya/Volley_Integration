/*
 * Copyright © 2016, National Media Group Ltd. All rights reserved.
 * Written under contract by Robosoft Technologies Pvt. Ltd.
 */

package robosoft.samplevolleyintegration;

import android.content.Context;
import android.support.annotation.VisibleForTesting;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.Volley;
import com.google.gsonhtcfix.Gson;


import java.io.File;


public class CLCVolley {

    private static RequestQueue mRequestQueue;
    private static RequestQueue mImageRequestQueue;
    private static String TAG = "TAG";
    private static Context mContext;


    /**
     * initialize Volley
     */
    public static void init(Context context) {
        mContext = context;
        getRequestQueue(context);
        mImageRequestQueue = Volley.newRequestQueue(context, true);
        //ImageCacheManager.INSTANCE.initImageCache();
    }

    public static <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        req.setShouldCache(true);
        getRequestQueue(mContext).add(req);
    }

    public  static <T> Request<T> addSyncRequest(Request<T> req){
        return getRequestQueue(mContext).add(req);
    }


    public static RequestQueue getRequestQueue(Context context) {
        if (mRequestQueue != null) {
            return mRequestQueue;
        } else {
            return mRequestQueue = Volley.newRequestQueue(context, new HurlStack(),false);
        }
    }

    public static RequestQueue getImageRequestQueue() {

        if (mImageRequestQueue != null) {
            return mImageRequestQueue;
        } else {
            throw new IllegalStateException("Image RequestQueue not initialized");
        }
    }

}
