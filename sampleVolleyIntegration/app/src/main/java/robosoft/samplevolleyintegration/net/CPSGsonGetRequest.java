package robosoft.samplevolleyintegration.net;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.google.gsonhtcfix.Gson;


import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import robosoft.samplevolleyintegration.CPSConstants;
import robosoft.samplevolleyintegration.entity.CPSIllegalCodeError;
import robosoft.samplevolleyintegration.entity.IPSDataModel;


public class CPSGsonGetRequest extends Request<IPSDataModel> {

    private final Listener<IPSDataModel> mListener;
    private IPSDataModel mDataModel;
    private Gson mGson = null;
    private Map<String, String> mHeaders;
    private String TAG = CPSGsonGetRequest.class.getName();
    public static final int MY_SOCKET_TIMEOUT_MS = 60000;
    private String mUrl;
    private String mRequestBody;
    public static final String HEADER_AUTHORIZATION = "Authorization";
    public static final String AUTHORIZATION = "Basic cGVwc2l2dHNwb2M6cGVwc2lAJDEyMw==";
    /**
     * Charset for request.
     */
    private static final String PROTOCOL_CHARSET = "utf-8";


    public CPSGsonGetRequest(String url, Listener<IPSDataModel> listener, ErrorListener
            errorListener, IPSDataModel model) {
        super(Method.GET, url, errorListener);
        mListener = listener;
        mDataModel = model;
        mGson = new Gson();
        mUrl = url;
        Log.d(TAG  , "Request Url: " +mUrl);
    }

    public CPSGsonGetRequest(String url, Listener<IPSDataModel> listener, ErrorListener
            errorListener, IPSDataModel model, Map<String, String> headers) {
        super(Method.GET, url, errorListener);
        mListener = listener;
        mDataModel = model;
        mGson = new Gson();
        mHeaders = headers;
        mUrl = url;
    }

    public CPSGsonGetRequest(String url, Listener<IPSDataModel> listener, ErrorListener
            errorListener, IPSDataModel model, Map<String, String> headers, String requestBody) {
        super(Method.GET, url, errorListener);
        mListener = listener;
        mDataModel = model;
        mGson = new Gson();
        mHeaders = headers;
        mUrl = url;
        mRequestBody = requestBody;
    }



    @Override
    protected void deliverResponse(IPSDataModel model) {
        mListener.onResponse(model);

    }

    @Override
    public void deliverError(VolleyError error) {
        super.deliverError(error);
    }

    @Override
    public Request<?> setRetryPolicy(RetryPolicy retryPolicy) {
        retryPolicy = new DefaultRetryPolicy(MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        return super.setRetryPolicy(retryPolicy);
    }


    @Override
    protected Response<IPSDataModel> parseNetworkResponse(NetworkResponse response) {
        String jsonString = new String(response.data);


        int statusCode = response.statusCode;
        Log.d(TAG  , "json Response String: " + jsonString+" statusCode:"+response.statusCode);
        try {

            if (statusCode == CPSConstants.ERROR_CODE_403) {
                statusCode = CPSConstants.ERROR_CODE_401;
            }
            if (statusCode == CPSConstants.ERROR_CODE_401 || statusCode == CPSConstants.ERROR_CODE_410) {
                throw new IllegalErrorCode(jsonString);
            }
            if (statusCode == CPSConstants.ERROR_CODE_499 || statusCode == CPSConstants.ERROR_CODE_502
                    || statusCode == CPSConstants.ERROR_CODE_503 || statusCode == CPSConstants.ERROR_CODE_504) {
                throw new IllegalErrorCode(jsonString);
            }

            if (jsonString == null || jsonString.trim().length() == 0) {
                throw new IllegalStateException();
            }
            try {

                CPSIllegalCodeError errorStatus = new CPSIllegalCodeError();
                errorStatus = mGson.fromJson(jsonString, errorStatus.getClass());

                if (errorStatus != null && errorStatus.getStatusError() != null && errorStatus.getStatusError().getmResult() != null
                        && errorStatus.getStatusError().getmResult().equalsIgnoreCase(CPSConstants.FAILURE)) {
                    VolleyError volleyError = new VolleyError(CPSConstants.FAILURE_ERROR);
                    volleyError.setUrl(mUrl);
                    if (errorStatus != null && errorStatus.getStatusError() != null && errorStatus.getStatusError().getmMessage() != null) {
                        volleyError.setAlertMessage(errorStatus.getStatusError().getmMessage().getMessage());
                        volleyError.setmAlertTitle(errorStatus.getStatusError().getmMessage().getTitle());
                    }

                    return Response.error(volleyError);
                }

            } catch (Exception ex) {
                Log.e("exp",ex.getMessage());
            }

            return Response.success((IPSDataModel) mGson.fromJson(jsonString, mDataModel.getClass()), getCacheEntry());

        } catch (IllegalErrorCode e) {
            e.printStackTrace();
            VolleyError volleyError = new VolleyError(String.valueOf(statusCode));
            volleyError.setUrl(mUrl);
            CPSIllegalCodeError errorStatus = new CPSIllegalCodeError();
            try {
                errorStatus = mGson.fromJson(jsonString, errorStatus.getClass());
                if (errorStatus != null && errorStatus.getStatusError() != null && errorStatus.getStatusError().getmMessage() != null) {
                    volleyError.setAlertMessage(errorStatus.getStatusError().getmMessage().getMessage());
                    volleyError.setmAlertTitle(errorStatus.getStatusError().getmMessage().getTitle());
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return Response.error(volleyError);
        } catch (Exception exception) {
            exception.printStackTrace();
            VolleyError volleyError = new VolleyError(CPSConstants.PARSE_ERROR);
            volleyError.setUrl(mUrl);
            return Response.error(volleyError);
        }
    }



    @Override
    public Map<String, String> getHeaders() {
        try {
            return mHeaders != null ? mHeaders : super.getHeaders();
        } catch (AuthFailureError exception) {
            exception.printStackTrace();
            return null;
        }
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        try {
            return mRequestBody == null ? null : mRequestBody.getBytes(PROTOCOL_CHARSET);
        } catch (UnsupportedEncodingException uee) {

            return null;
        }
    }


}
