package robosoft.samplevolleyintegration.net;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.google.gsonhtcfix.Gson;


import java.io.UnsupportedEncodingException;
import java.util.Map;

import robosoft.samplevolleyintegration.CPSConstants;
import robosoft.samplevolleyintegration.entity.CPSIllegalCodeError;
import robosoft.samplevolleyintegration.entity.IPSDataModel;

public class CPSGsonPostRequest extends Request<IPSDataModel> {

    private final Listener<IPSDataModel> mListener;
    private IPSDataModel mDataModel;
    private final Gson mGson;
    private Map<String, String> mHeaders;
    private Map<String, String> mParams;
    private final String mRequestBody;
    private String TAG = CPSGsonPostRequest.class.getName();
    private String mUrl;

    /**
     * Charset for request.
     */
    private static final String PROTOCOL_CHARSET = "utf-8";

    public static final int MY_SOCKET_TIMEOUT_MS = 60000;


    /**
     * Content type for request.
     */
    private static final String PROTOCOL_CONTENT_TYPE =
            String.format("application/json; charset=%s", PROTOCOL_CHARSET);

    public CPSGsonPostRequest(String url, Listener<IPSDataModel> listener, ErrorListener
            errorListener, IPSDataModel model, Map<String, String> params,
                              Map<String, String> headers, String requestBody) {
        super(Method.POST, url, errorListener);
        mListener = listener;
        mDataModel = model;
        mGson = new Gson();
        mParams = params;
        mHeaders = headers;
        mRequestBody = requestBody;
        mUrl = url;
		Log.d(TAG, "Request Url: " + mUrl);
        Log.d(TAG  , "Request Body: " +mRequestBody);
	}


    @Override
    public Request<?> setRetryPolicy(RetryPolicy retryPolicy) {
        retryPolicy = new DefaultRetryPolicy(MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        return super.setRetryPolicy(retryPolicy);
    }

    @Override
    protected void deliverResponse(IPSDataModel model) {
        mListener.onResponse(model);
    }


    @Override
    protected Response<IPSDataModel> parseNetworkResponse(NetworkResponse response) {
        String jsonString = new String(response.data);
        Log.d(TAG  , "json Response String: " + jsonString+" statusCode:"+response.statusCode);
        int statusCode = response.statusCode;
        try {
            if (statusCode == CPSConstants.ERROR_CODE_403) {
                statusCode = CPSConstants.ERROR_CODE_401;
            }

            if(statusCode == CPSConstants.ERROR_CODE_401 || statusCode == CPSConstants.ERROR_CODE_410 ){
                throw new IllegalErrorCode(jsonString);
            }
            if(statusCode == CPSConstants.ERROR_CODE_449 || statusCode == CPSConstants.ERROR_CODE_499 || statusCode == CPSConstants.ERROR_CODE_502
                    || statusCode == CPSConstants.ERROR_CODE_503 || statusCode == CPSConstants.ERROR_CODE_504) {
                throw new IllegalErrorCode(jsonString);
            }
            if (statusCode == CPSConstants.ERROR_CODE_400 ) {
                throw new IllegalErrorCode(jsonString);
            }
            if(jsonString == null || jsonString.trim().length() == 0) {
                throw new IllegalStateException();
            }
            try {

                CPSIllegalCodeError errorStatus = new CPSIllegalCodeError();
                errorStatus = mGson.fromJson(jsonString, errorStatus.getClass());
                if(errorStatus != null && errorStatus.getStatusError() != null && errorStatus.getStatusError().getmResult()!= null
                        && errorStatus.getStatusError().getmResult().equalsIgnoreCase(CPSConstants.FAILURE)) {
                    VolleyError volleyError = new VolleyError(CPSConstants.FAILURE_ERROR);
                    volleyError.setUrl(mUrl);
                    if(errorStatus != null  && errorStatus.getStatusError() != null && errorStatus.getStatusError().getmMessage() != null) {
                        volleyError.setAlertMessage(errorStatus.getStatusError().getmMessage().getMessage());
                        volleyError.setmAlertTitle(errorStatus.getStatusError().getmMessage().getTitle());
                    }
                    return Response.error(volleyError);
                }

            } catch(Exception ex){

            }

            return Response.success((IPSDataModel)mGson.fromJson(jsonString, mDataModel.getClass()), getCacheEntry());
        } catch(IllegalErrorCode e){
            e.printStackTrace();
            VolleyError volleyError = new VolleyError(String.valueOf(statusCode));
            volleyError.setUrl(mUrl);
            CPSIllegalCodeError errorStatus = new CPSIllegalCodeError();
            try {
                errorStatus = mGson.fromJson(jsonString, errorStatus.getClass());
                if(errorStatus != null  && errorStatus.getStatusError() != null && errorStatus.getStatusError().getmMessage() != null) {
                    volleyError.setAlertMessage(errorStatus.getStatusError().getmMessage().getMessage());
                    volleyError.setmAlertTitle(errorStatus.getStatusError().getmMessage().getTitle());
                }
            }catch(Exception ex) {
                ex.printStackTrace();
            }
            return Response.error(volleyError);
        } catch (Exception exception) {
            exception.printStackTrace();
            VolleyError volleyError = new VolleyError(CPSConstants.PARSE_ERROR);
            volleyError.setUrl(mUrl);
            return Response.error(volleyError);
        }


    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        return mParams != null ? mParams : super.getParams();
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return mHeaders != null ? mHeaders : super.getHeaders();
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
		try {
			return mRequestBody == null ? null : mRequestBody.getBytes(PROTOCOL_CHARSET);
		} catch (UnsupportedEncodingException uee) {

			return null;
		}
	}


	@Override
	public String getBodyContentType() {
		 return null;
	}

    @Override
    public String getPostBodyContentType() {
        return getBodyContentType();
    }

    /**
     * @throws AuthFailureError
     * @deprecated Use {@link #getBody()}.
     */
    @Override
    public byte[] getPostBody() throws AuthFailureError {
        return getBody();
    }


}

